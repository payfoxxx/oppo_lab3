#pragma once

using namespace std;

void parsing(Container &container);
bool correct_data(string buff);
bool correct_data_car(string buff);
void parse_truck(string buff, Truck& truck_buf);
void parse_bus(string buf, Bus& bus_buf);
void parse_car(string buf, Car& car_buf);
