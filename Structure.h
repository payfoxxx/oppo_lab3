#pragma once
#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <algorithm>

using namespace std;

class Transport {
	virtual void Say() {
		cout << "Virtual" << endl;
	}
public:
	int power; //�������� ���������
	string country; //������ �������������
	string brand; //����� ������
	string type;
};

class Truck : public Transport {
public:
	int lift; //����������������

	Truck() {

	}
	Truck(int &lift_b, int &power_b, string &country_b, string &brand_b) {
		type = "truck";
		this->lift;
		this->power;
		this->country;
		this->brand;
		this->type;
	}

	void Display() {
		cout << "��������" << "\n\t����������������: " << this->lift << "\n\t��������: " << this->power << "\n\t�������������: " << this->country << "\n\t�����: " << this->brand << endl;
	}
};

class Bus : public Transport {
public:
	short passengers; //��������������������

	Bus() {

	}

	Bus(short &passengers_b, int &power_b, string &country_b, string &brand_b) {
		passengers = passengers_b;
		power = power_b;
		country = country_b;
		brand = brand_b;
		type = "bus";
		this->passengers;
		this->power;
		this->country;
		this->brand;
		this->type;
	}
	void Display() {
		cout << "�������" << "\n\t�����������: " << this->passengers << "\n\t��������: " << this->power << "\n\t�������������: " << this->country << "\n\t�����: " << this->brand << endl;
	}
};

class Car : public Transport {
public:
	string bodyType; //��� ������

	Car() {

	}

	Car(string &bodyType_b, int &power_b, string &country_b, string &brand_b) {
		bodyType = bodyType_b;
		power = power_b;
		country = country_b;
		brand = brand_b;
		type = "car";
		this->bodyType;
		this->power;
		this->country;
		this->brand;
		this->type;
	}
	void Display() {
		cout << "������" << "\n\t��� ������: " << this->bodyType << "\n\t��������: " << this->power << "\n\t�������������: " << this->country << "\n\t�����: " << this->brand << endl;
	}
};


class Container {  //����� ���������� ��� �������� ������
public:
	vector<Transport*> Trans;

	void View() {
		int position = 0;
		for (Transport* data : this->Trans) {
			position++;
			if (dynamic_cast<Truck*>(data)) {
				cout << position << ")";
				((Truck*)data)->Display();
			}
			else if (dynamic_cast<Bus*>(data)) {
				cout << position << ")";
				((Bus*)data)->Display();
			}
			else if (dynamic_cast<Car*>(data)) {
				cout << position << ")";
				((Car*)data)->Display();
			}
		}
	cout << endl << "���������� ���������: " << position << endl;
	}


	void Sort() {  //���������� �������
		int buf, j;
		for (int i = 0; i < this->Trans.size(); i++) {
			j = i;
			for (int k = i; k < this->Trans.size(); k++) {
				int power1 = this->Trans[k]->power;
				int power2 = this->Trans[j]->power;
				if (power2 > power1)
					j = k;
			}
			swap(this->Trans[i], this->Trans[j]);
		}
	}

	void Delete_Type(string type) {
		for (int i = 0; i < Trans.size(); i++) {
			if (Trans[i]->type == type) {
				Trans.erase(Trans.begin() + i);
				i--;
			}
		}
	}
	~Container() {
		for (Transport* var : this->Trans)
			delete var;
	}
};
