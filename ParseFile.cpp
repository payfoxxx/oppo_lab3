#include "pch.h"
#include "Structure.h"
#include "ParseFile.h"

using namespace std;

bool correct_data(string buff) {
	if (buff[0] == ' ') {
		buff.erase(0, 1);
	}
	else return false;
	if (buff[0] == ' ')
		return false;
	int i = 0;
	while (buff[i] != ' ') {
		if (buff[i] >= '0' && buff[i] <= '9')
			i++;
		else return false;
	}
	i++;
	if (buff[i] != ' ') {
		while (buff[i] != ' ') {
			if (buff[i] >= '0' && buff[i] <= '9')
				i++;
			else return false;
		}
	}
	i++;
	int pos;
	if (buff[i] != ' ') {
		if (buff[i] == '\"') {
			pos = buff.find('\"', i + 1);
			if (pos > 0 && buff[pos + 1] == ' ') {
				i++;
				while (i < pos - 1) {
					if (buff[i] >= 'A' && buff[i] <= 'z' || buff[i] == ' ')
						i++;
					else return false;
				}
			}
			else return false;
		}
		else
			while (buff[i] != ' ') {
				if (buff[i] >= 'A' && buff[i] <= 'z')
					i++;
				else return false;
			}
	}
	else return false;
	i++;
	if (buff[i] != ' ') {
		if (buff[i] == '\"') {
			pos = buff.find('\"', i);
			if (pos > 0 && buff[pos + 1] == ' ') {
				while (i < pos) {
					if (buff[i] >= 'A' && buff[i] <= 'z')
						i++;
					else return false;
				}
			}
			else return false;
		}
		else
			while (buff[i] != buff[buff.size() - 1]) {
				if (buff[i] >= 'A' && buff[i] <= 'z')
					i++;
				else return false;
			}
	}
	else return false;
	return true;
}

bool correct_data_car(string buff) {
	if (buff[0] == ' ') {
		buff.erase(0, 1);
	}
	else return false;
	if (buff[0] == false)
		return false;
	int i = 0;
	int pos;
	if (buff[i] == '\"') {
		pos = buff.find('\"', i);
		if (pos > 0 && buff[pos + 1] == ' ') {
			while (i < pos) {
				if (buff[i] >= 'A' && buff[i] <= 'z')
					i++;
				else return false;
			}
		}
		else return false;
	}
	else
		while (buff[i] != ' ') {
			if (buff[i] >= 'A' && buff[i] <= 'z')
				i++;
			else return false;
		}
	i++;
	if (buff[i] != ' ') {
		while (buff[i] != ' ') {
			if (buff[i] >= '0' && buff[i] <= '9')
				i++;
			else return false;
		}
	}
	else return false;
	i++;
	if (buff[i] != ' ') {
		if (buff[i] == '\"') {
			pos = 0;
			pos = buff.find('\"', i);
			if (pos > 0 && buff[pos + 1] == ' ') {
				while (i < pos) {
					if (buff[i] >= 'A' && buff[i] <= 'z')
						i++;
					else return false;
				}
			}
			else return false;
		}
		else
			while (buff[i] != ' ') {
				if (buff[i] >= 'A' && buff[i] <= 'z')
					i++;
				else return false;
			}
	}
	else return false;
	i++;
	if (buff[i] != ' ') {
		if (buff[i] == '\"') {
			pos = 0;
			pos = buff.find('\"', i);
			if (pos > 0 && buff[pos + 1] == ' ') {
				while (i < pos) {
					if (buff[i] >= 'A' && buff[i] <= 'z')
						i++;
					else return false;
				}
			}
			else return false;
		}
		else
			while (buff[i] != buff[buff.size() - 1]) {
				if (buff[i] >= 'A' && buff[i] <= 'z')
					i++;
				else return false;
			}
	}
	else return false;
	return true;
}

void parse_truck(string buf, Truck& truck_buf) {
	string buffer;
	int spacesCol = 0;
	int space = buf.find(' ');
	int i;
	for (i = 0; i < space; i++)
		buffer += buf[i];
	truck_buf.lift = stoi(buffer);
	buf.erase(0, i + 1);
	buffer.clear();
	spacesCol++;
	space = buf.find(' ');
	for (i = 0; i < space; i++)
		buffer += buf[i];
	truck_buf.power = stoi(buffer);
	buf.erase(0, i + 1);
	buffer.clear();
	i = 0;

	if (buf[i] == '\"') {
		space = buf.find('\"', i + 1);
		space++;
	}
	else space = buf.find(' ');
	for (i = 0; i < space; i++)
		buffer += buf[i];
	truck_buf.country = buffer;
	buf.erase(0, i + 1);
	truck_buf.brand = buf;
}

void parse_bus(string buf, Bus& bus_buf) {
	string buffer;
	int spacesCol = 0;
	int space = buf.find(' ');
	int i;
	for (i = 0; i < space; i++)
		buffer += buf[i];
	bus_buf.passengers = stoi(buffer);
	buf.erase(0, i + 1);
	buffer.clear();
	spacesCol++;
	space = buf.find(' ');
	for (i = 0; i < space; i++)
		buffer += buf[i];
	bus_buf.power = stoi(buffer);
	buf.erase(0, i + 1);
	buffer.clear();
	if (buf[i] == '\"') {
		space = buf.find('\"');
		space++;
	}
	else space = buf.find(' ');
	for (i = 0; i < space; i++)
		buffer += buf[i];
	bus_buf.country = buffer;
	buf.erase(0, i + 1);
	bus_buf.brand = buf;

}

void parse_car(string buf, Car& car_buf) {
	string buffer;
	int i = 0;
	int space;
	if (buf[i] == '\"') {
		space = buf.find('\"');
		space++;
	}
	else space = buf.find(' ');
	for (i = 0; i < space; i++)
		buffer += buf[i];
	car_buf.bodyType = buffer;
	buf.erase(0, i + 1);
	buffer.clear();
	space = buf.find(' ');
	for (i = 0; i < space; i++)
		buffer += buf[i];
	car_buf.power = stoi(buffer);
	buf.erase(0, i + 1);
	buffer.clear();
	if (buf[i] == '\"') {
		space = buf.find('\"');
		space++;
	}
	else space = buf.find(' ');
	for (i = 0; i < space; i++)
		buffer += buf[i];
	car_buf.country = buffer;
	buf.erase(0, i + 1);
	car_buf.brand = buf;
}

void parsing(Container &container) {
	string buff;
	bool error;
	ifstream file("in.txt");
	int col = 0;
	if (!file) {
		cout << "����� �� �������!";
	}
	else {
		while (!file.eof()) {
			col++;
			file >> buff;
			transform(buff.begin(), buff.end(), buff.begin(), tolower);
			error = false;
			if (buff == "truck:") {
				getline(file, buff);
				if (correct_data(buff) == true) {
					Truck *truck_buf = new Truck;
					buff.erase(0, 1);
					parse_truck(buff, *truck_buf);
					container.Trans.push_back((truck_buf));
				} else
					error = true;
			}
			if (buff == "bus:") {
				getline(file, buff);
				if (correct_data(buff) == true) {
					Bus *bus_buf = new Bus;
					buff.erase(0, 1);
					parse_bus(buff, *bus_buf);
					container.Trans.push_back(bus_buf);
				}
				else
					error = true;
			}
			if (buff == "car:") {
				getline(file, buff);
				if (correct_data_car(buff) == true) {
					Car *car_buf = new Car;
					buff.erase(0, 1);
					parse_car(buff, *car_buf);
					container.Trans.push_back(car_buf);
				}
				else
					error = true;
			}
			if (error == true)
				cout << "������ � ������ " << col << endl;
		}
		file.close();
	}
}



