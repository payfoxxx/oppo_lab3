#include "pch.h"
#include "Structure.h"
#include "ParseFile.h"
#define _CRTDBG_MAP_ALLOC
#include <crtdbg.h>
#define DBG_NEW new( _NORMAL_BLOCK , __FILE__ , __LINE__ )
#define newDBG_NEW

using namespace std;

void print_menu() {
	system("cls");
	cout << "Выберите действие" << endl;
	cout << "1. Загрузить с файла" << endl;
	cout << "2. Вывести на экран" << endl;
	cout << "3. Отсортировать" << endl;
	cout << "4. Удалить тип" << endl;
	cout << "0. Выход" << endl;
}

void delete_type(string &whichType) {
	system("cls");
	cout << "Какой тип удалить?" << endl;
	cout << "1. Автобус" << endl;
	cout << "2. Грузовик" << endl;
	cout << "3. Машина" << endl;
	int k;
	cin >> k;
	switch (k) {
	case 1:
		whichType = "bus";
		break;
	case 2:
		whichType = "truck";
		break;
	case 3:
		whichType = "car";
		break;
	default:
		cout << "Ошибка" << endl;
		break;
	}
	system("pause");
}

void run() {
	setlocale(LC_ALL, "rus");
	Container container;
	int menuPoint;
	string whichType;
	do {
		print_menu();
		cin >> menuPoint;
		switch (menuPoint) {
		case 1:
			system("cls");
			parsing(container);
			cout << "Done!" << endl;
			system("pause");
			break;
		case 2:
			system("cls");
			container.View();
			system("pause");
			break;
		case 3:
			system("cls");
			container.Sort();
			cout << "Done!" << endl;
			system("pause");
			break;
		case 4:
			system("cls");
			delete_type(whichType);
			container.Delete_Type(whichType);
			cout << "Done!" << endl;
			system("pause");
			break;
		case 0:
			system("cls");
			cout << "Работа завершена";
			break;
		default:
			system("cls");
			cout << "Некорректный пункт" << endl;
			system("pause");
			break;
		}
	} while (menuPoint != 0);
}

int main()
{
	run();
	_CrtDumpMemoryLeaks();
	return 0;
}

